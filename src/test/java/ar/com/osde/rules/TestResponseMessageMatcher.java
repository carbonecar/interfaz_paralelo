package ar.com.osde.rules;

import ar.com.osde.rules.paralelo.NivelValidez;
import ar.com.osde.rules.paralelo.Response;
import ar.com.osde.rules.paralelo.ResponseMatcher;
import ar.com.osde.rules.paralelo.ResponseMessageMatcher;
import ar.com.osde.rules.paralelo.ResponseRecord;
import ar.com.osde.rules.paralelo.ResponseRecord.SystemName;
import junit.framework.TestCase;

public class TestResponseMessageMatcher extends TestCase {

	public void testFirstResponsePos() {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		responseMessageMatcher.addRequest(
		        "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000");

		responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000",
		        ResponseRecord.SystemName.POS.getName());
		ResponseMatcher responseMatcher = responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ",
		        ResponseRecord.SystemName.SALUD_SOFT.getName());

		assertTrue(responseMatcher.isFullResult());

		assertEquals(SystemName.POS.getName(), responseMatcher.getResponseRecord().getFirstResponse().getSystemName());
		assertEquals(SystemName.SALUD_SOFT.getName(), responseMatcher.getResponseRecord().getSecondResponse().getSystemName());


	}

	
	

	public void testFirstResponseSS() {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		responseMessageMatcher.addRequest(
		        "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000");

		responseMessageMatcher.addResponse(
				"SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ",
				ResponseRecord.SystemName.SALUD_SOFT.getName());
		ResponseMatcher responseMatcher = responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000",
		        ResponseRecord.SystemName.POS.getName());

		assertTrue(responseMatcher.isFullResult());

		assertEquals(SystemName.SALUD_SOFT.getName(), responseMatcher.getResponseRecord().getFirstResponse().getSystemName());
		assertEquals(SystemName.POS.getName(), responseMatcher.getResponseRecord().getSecondResponse().getSystemName());


	}
	
	
	
	public void testLastRequestFirstResponsePOS() {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();


		responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000",
		        ResponseRecord.SystemName.POS.getName());

		responseMessageMatcher.addResponse(
				"SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ",
				ResponseRecord.SystemName.SALUD_SOFT.getName());

		ResponseMatcher responseMatcher = responseMessageMatcher.addRequest(
		        "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000");

		assertTrue(responseMatcher.isFullResult());

		assertEquals(SystemName.POS.getName(), responseMatcher.getResponseRecord().getFirstResponse().getSystemName());
		assertEquals(SystemName.SALUD_SOFT.getName(), responseMatcher.getResponseRecord().getSecondResponse().getSystemName());


	}
	public void testResponseRequestResponseIdentica() {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000",
		        "pos");
		responseMessageMatcher.addRequest(
		        "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000");
		ResponseMatcher responseMatcher = responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ",
		        "ss");

		assertTrue(responseMatcher.isFullResult());

	}

	public void testRequestResponseResponseIdentica() {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		responseMessageMatcher.addRequest(
		        "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000");
		responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000",
		        "pos");

		ResponseMatcher responseMatcher = responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ",
		        "ss");

		assertTrue(responseMatcher.isFullResult());

	}
	
	
	public void testRespuestaCierreIdentica() {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		responseMessageMatcher.addRequest(
		        "FRM  AF2304065123203852754000000           003F17200900000M20180125180719  000000                                                                      00000000000000            0010100                                           00                               00                               00                                                            2017120820180124   0000000000                                                               050857100000000    VA0000000                  \u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ");


		responseMessageMatcher.addResponse(
		        "FRM  AF23040651172009000000980000001        SOLICITUD DE Consulta RECHAZADA         SOLICITUD DE Consulta RECHAZADA CIERRE INEXSITENTE                              000000000000101      0000                                                                                 00000000                          00                          00                          00                          00    00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000  AF17200920180125    AF17200920180125",
		        "pos");

		
		ResponseMatcher responseMatcher = responseMessageMatcher.addResponse(
		        "FRM  AF23040651172009000000980000001        SOLICITUD DE CIERRE   RECHAZADA         SOLICITUD DE CIERRE   RECHAZADA NO HAY INFORMACION                              000000000000101     002                                                              N                                                            000000000000000000000000000000000000000000000000000000000000000000000000                                                                                                    ",
		        "ss");

		assertFalse(responseMatcher.isFullResult());

	}
	
	
	
	
	
	
	
	
	public void testResponseResponseRequestIdentica() {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000",
		        "pos");
		responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ",
		        "ss");

		ResponseMatcher responseMatcher = responseMessageMatcher.addRequest(
		        "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000");

		assertTrue(responseMatcher.isFullResult());

	}

	public void testResponseRequestResponseDiferenciaAptitud() {

		String responsePos = "SAF  AA60003813855403000000980000001        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000";
		String responseSS = "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ";

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		responseMessageMatcher.addResponse(responsePos, "pos");
		responseMessageMatcher.addRequest(
		        "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000");
		ResponseMatcher responseMatcher = responseMessageMatcher.addResponse(responseSS, "ss");

		assertTrue(responseMatcher.getNivelValidez().equals(NivelValidez.DIFERENCIA_APTITUD));

		assertNotNull(responseMatcher.getResponseRecord().getSecondResponse());
		assertTrue(!responseMatcher.getResponseRecord().getFirstResponse().getTextResponse()
		        .equals(responseMatcher.getResponseRecord().getSecondResponse()));

		assertTrue(responseMatcher.getResponseRecord().getFirstResponse().getTextResponse()
		        .contains("SAF  AA60003813855403000000980000001"));

		assertEquals(responsePos,
		        responseMatcher.getResponseRecord().getResonseBySystemName(SystemName.POS).getTextResponse());
		assertEquals(responseSS,
		        responseMatcher.getResponseRecord().getResonseBySystemName(SystemName.SALUD_SOFT).getTextResponse());
		assertNotSame(responseSS,
		        responseMatcher.getResponseRecord().getResonseBySystemName(SystemName.POS).getTextResponse());

	}

	public void testRequestResponseResponseDiferenciaAptitud() {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		responseMessageMatcher.addRequest(
		        "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000");
		responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000",
		        "ss");

		ResponseMatcher responseMatcher = responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000001        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ",
		        "pos");

		assertTrue(responseMatcher.getNivelValidez().equals(NivelValidez.DIFERENCIA_APTITUD));
		assertNotNull(responseMatcher.getResponseRecord().getSecondResponse());
		assertTrue(!responseMatcher.getResponseRecord().getFirstResponse().getTextResponse()
		        .equals(responseMatcher.getResponseRecord().getSecondResponse()));
		assertTrue(responseMatcher.getResponseRecord().getFirstResponse().getTextResponse()
		        .contains("SAF  AA60003813855403000000980000000"));

	}

	public void testResponseResponseRequestDiferenciaAptitud() {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000001        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000",
		        "pos");
		responseMessageMatcher.addResponse(
		        "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ",
		        "ss");

		ResponseMatcher responseMatcher = responseMessageMatcher.addRequest(
		        "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000");

		assertTrue(responseMatcher.getNivelValidez().equals(NivelValidez.DIFERENCIA_APTITUD));

		assertNotNull(responseMatcher.getResponseRecord().getSecondResponse());
		assertTrue(!responseMatcher.getResponseRecord().getFirstResponse().getTextResponse()
		        .equals(responseMatcher.getResponseRecord().getSecondResponse()));

		assertTrue(responseMatcher.getResponseRecord().getFirstResponse().getTextResponse()
		        .contains("SAF  AA60003813855403000000980000001"));

	}

	public void testOldResponses() throws InterruptedException {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		String response1 = "SAF  AA60003813855403000000980000001        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000";

		String response2 = "SAF  AA60003813855401000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ";
		String request = "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000";

		responseMessageMatcher.addRequest(request);
		responseMessageMatcher.addResponse(response1, "pos");

		Thread.sleep(30 * 1000);
		ResponseMatcher responseMatcher = responseMessageMatcher.addResponse(response2, "ss");

		assertNull(responseMatcher.getNivelValidez());

	}

	public void testResponseTime() throws InterruptedException {

		ResponseMessageMatcher responseMessageMatcher = new ResponseMessageMatcher();

		String response1 = "SAF  AA60003813855403000000980000001        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                      00000";

		String response2 = "SAF  AA60003813855403000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ";
		String request = "SAF  AA600038133061524006700000061752976001001A85540300000M20120510182934  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-05-10-18.26.37.771000";

		String response3 = "SAF  AA60003813855413000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                PLAN 2 310  NO GRAV.                    0000000000001012 310O0000                                                  NU�EZ MARIANA LAURA          F19770416                          00                          00                          00                          00   N00000000000000000000000                                        00000000000000000000000                                        00000000000000000000000                                                                                00000000000 0000000000     ";

		responseMessageMatcher.addRequest(request);
		responseMessageMatcher.addResponse(response1, "pos");

		Thread.sleep(70 * 1000);
		// responseMessageMatcher.addResponse(response3, "ss");
		ResponseMatcher responseMatcher = responseMessageMatcher.addResponse(response2, "ss");

		Response response = responseMatcher.getResponseRecord().getResonseBySystemName(SystemName.SALUD_SOFT);
		assertTrue(response.getResponseTime() >= 20000);

	}

}
