package ar.com.osde.rules;

import ar.com.osde.rules.paralelo.RequestRecord;
import junit.framework.TestCase;

public class RequestRecordTestCase extends TestCase {

	public void testBuildRecord() {

		RequestRecord requestRecord = new RequestRecord(
		        "SAF  AA600069103356133060900000060964547101001A36874700000M20120307163922  0000000000000000       0000000000       0000000000       0000000000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          2012-03-07-16.37.58.694000");

		assertEquals("AA",requestRecord.getTransactionKey().getCodigoOperador());
		assertEquals(60, requestRecord.getTransactionKey().getFilialPos());
		assertEquals(0, requestRecord.getTransactionKey().getDelegacionPos());
		assertEquals(6910, requestRecord.getTransactionKey().getNroPos());
		assertEquals(368747, requestRecord.getTransactionKey().getNroTx());
		
		assertEquals(1, requestRecord.getTransactionKey().getNroSecuenciaMensaje());
		assertEquals(1, requestRecord.getTransactionKey().getNroTotalMensaje());

		
	}
}
