package ar.com.osde.rules;

import ar.com.osde.rules.paralelo.ResponseRecord;
import junit.framework.Assert;
import junit.framework.TestCase;

public class SystemNameTestCase extends TestCase {
	
	public void testName(){
		
		Assert.assertEquals("se encontro: "+ResponseRecord.SystemName.POS,"pos",ResponseRecord.SystemName.POS.getName());
		Assert.assertEquals("se encontro: "+ResponseRecord.SystemName.SALUD_SOFT,"ss",ResponseRecord.SystemName.SALUD_SOFT.getName());
	}

}
