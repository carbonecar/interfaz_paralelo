package ar.com.osde.rules;

import ar.com.osde.rules.paralelo.GeoCodingBuilder;
import junit.framework.TestCase;

public class GeoCodingTestCase extends TestCase {

	private static final String REQUEST_1 = "FRM  AF250000352010225039800000061146050403002F04179000000M20170928184609  000000                                                                    MB11029620170928            0010135                       5175262779217500747401                               00                               00                                                            0000000000000000   0000000000                                                               057585120170927       000000033575                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ";

	public void testGetGeocoding() {
		assertEquals("69yc34v7m", GeoCodingBuilder.getGeoHashByFilialID(REQUEST_1));
	}
}
