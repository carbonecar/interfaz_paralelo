package ar.com.osde.rules.paralelo;

public class RequestRecord {

	private String request;
	private TransactionKey transactionKey;
	private long timestamp=0;
	
	
	public static final int CODIGO_OPERADOR = 0;
	public static final int FILIAL = 1;
	public static final int DELEGACION = 2;
	public static final int NRO_POS = 3;
	public static final int NRO_TRX = 4;

	public static final int NRO_SEC_MENSAJE = 5;
	public static final int NRO_TOTAL_MENSAJE = 6;

	public RequestRecord(String request) {
		this.request = request;
		this.transactionKey = new TransactionKey(request);
		
		this.timestamp=System.currentTimeMillis();
	}

	public long getTimestamp(){
		return this.timestamp;
	}
	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public TransactionKey getTransactionKey() {
		return transactionKey;
	}

	public void setTransactionKey(TransactionKey transactionKey) {
		this.transactionKey = transactionKey;
	}

}
