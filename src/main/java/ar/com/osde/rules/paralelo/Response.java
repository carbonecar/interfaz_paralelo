package ar.com.osde.rules.paralelo;

public class Response {

	private String textResponse;
	private String systemName;

	private long timestamp;
	private long responseTime;


	public Response(String textResponse, String systemName) {
		super();
		this.textResponse = textResponse;
		this.systemName = systemName;
		this.timestamp=System.currentTimeMillis();
	}

	public String getTextResponse() {
		return textResponse;
	}

	public void setTextResponse(String textResponse) {
		this.textResponse = textResponse;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getResponseTime() {
		return responseTime;
	}

	
	/**
	 * 
	 * @param timestamp where the request was received 
	 */
	public void measureResposneTime(long timestamp) {
		this.responseTime=this.timestamp-timestamp;
	}

}
