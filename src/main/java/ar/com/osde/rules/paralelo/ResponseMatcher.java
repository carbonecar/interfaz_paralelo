package ar.com.osde.rules.paralelo;

/**
 * Clase para determinar el resultado del matcheo
 * 
 * @author VA27789605
 * 
 */
public class ResponseMatcher {

    private boolean wasMatched = false;
    private boolean result = true;

    private boolean fullResult = false;

    private boolean porcentajes = false;
    
    private boolean onlyAdvertenciasFail=false;

	private ResponseRecord responseRecord;
    
    
    public ResponseRecord getResponseRecord() {
		return responseRecord;
	}

	public void setResponseRecord(ResponseRecord responseRecord) {
		this.responseRecord = responseRecord;
	}

	public boolean isOnlyAdvertenciasFail() {
        return onlyAdvertenciasFail;
    }

    public void setOnlyAdvertenciasFail(boolean onlyAdvertenciasFail) {
        this.onlyAdvertenciasFail = onlyAdvertenciasFail;
    }

    /**
     * Los porcentajes fueron identicos
     */
    public boolean isPorcentajes() {
        return porcentajes;
    }

    public void setPorcentajes(boolean porcentajes) {
        this.porcentajes = porcentajes;
    }

    public boolean isWasMatched() {
        return wasMatched;
    }

    public void setWasMatched(boolean wasMatched) {
        this.wasMatched = wasMatched;
    }

    /**
     * Si el resultado parcial fue correcto
     * 
     * @return
     */
    public boolean isResult() {
        return result;
    }

    /**
     * Valida que el resultado (apto o no apto sea igual)
     * @param result
     */
    public void setResult(boolean result) {
        this.result = result;
    }

    public void setFullResult(boolean result) {
        this.fullResult = result;
    }

    /**
     * Si el resultado entero fue correcto
     * 
     * @return
     */
    public boolean isFullResult() {
        return fullResult;
    }

    public NivelValidez getNivelValidez() {
        NivelValidez nivelValidez = null;
        if (this.isWasMatched()) {
            if (this.isFullResult()) {
                nivelValidez = NivelValidez.VALIDAS ;
            } else {
                if (this.isResult() == false) {
                    nivelValidez = NivelValidez.DIFERENCIA_APTITUD;
                } else {
                    if (!this.isPorcentajes()) {
                        nivelValidez = NivelValidez.DIFERENCIA_COBERTURA;
                    } else {
                        if(this.onlyAdvertenciasFail){
                            nivelValidez = NivelValidez.VALIDAS_CON_DIFERENCIA_N1;
                        }else{
                            nivelValidez = NivelValidez.VALIDAS_CON_DIFERENCIA;
                        }
                    }

                }

            }
        }
        return nivelValidez;
    }

}
