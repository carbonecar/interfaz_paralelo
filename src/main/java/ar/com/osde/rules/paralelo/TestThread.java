package ar.com.osde.rules.paralelo;

public class TestThread implements Runnable {

	private ResponseMessageMatcher matcher;

	public TestThread(ResponseMessageMatcher matcher) {
		this.matcher = matcher;
	}

	public static void main(String[] args) {
		ResponseMessageMatcher matcher = new ResponseMessageMatcher();
		new Thread(new TestThread(matcher)).start();
		new Thread(new TestThread(matcher)).start();
	}

	public void run() {
		String messageRequest = "02236IT600000473368645193961053161280938101101A68876202236M20120316171858XX0000000000000000       0000000000       0000000000       0000000000         00000000000000            001010000000000000000000000000                                                                                                                                 00000000000000000                                                                                                                                                                                                                                                                                                         2012-03-16-17.17.23.356000";
		String messageResponse = "02236IT60000047688762000000980223600        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                                                        000        1 1 210                                                                                                                                                                                                                                                           ";
		matcher.addResponse(messageResponse,"pos");
		matcher.addRequest(messageRequest);
		matcher.addResponse(messageResponse,"ss");

	}

}
