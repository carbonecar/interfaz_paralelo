package ar.com.osde.rules.paralelo;

import java.io.PrintWriter;

public class MainApp {

	public static ResponseRecord createResponseRecord(String response, String systemName) {
		ResponseRecord responseRecord = createResponseRecord(response);
		responseRecord.setFirstResponse(new Response(response, systemName));
		return responseRecord;
	}

	public static ResponseRecord createResponseRecord(String responseString) {
		ResponseRecord responseRecord = new ResponseRecord(responseString);
		return responseRecord;
	}

	/**
	 * Devuelve la clave
	 * 
	 * @deprecated
	 * @param line
	 * @return
	 */
	public static String[] splitMQFile(String line) {
		String[] splitedResponse = new String[6];

		splitedResponse[5] = line.substring(34, 36); // resultado
		return splitedResponse;
	}

	private static void writeRecord(PrintWriter writer, ResponseRecord record, String query) {
		writer.print("		Operador: " + record.getTxKey().getCodigoOperador());
		writer.print("	Filial: " + record.getTxKey().getFilialPos());
		writer.print(" Delegacion: " + record.getTxKey().getDelegacionPos());
		writer.print(" NroPos: " + record.getTxKey().getNroPos());
		writer.print(" NroTx: " + record.getTxKey().getNroTx());
		writer.print("		query: " + query);
	}

	public static String getQuery(ResponseRecord record) {
		StringBuffer myQuery = new StringBuffer(
		        "SELECT distinct PRFECT AS FECHA_TX,PRHORT AS HORA_TX,PROPED AS OPERADOR,PRFILP AS FILIAL ,PRDELP AS DELEGACION,PRNROP AS NRO_POS,PRSYST AS NRO_TX,R_LEYY RESULTADO");
		myQuery.append(",DEPRES as prestacion,R_LEYX FROM reglas.POREGP00 ,reglas.PODETP00,reglas.PORTAP00 ");

		myQuery.append(
		        " where PROPED=DEOPED AND PRFILP=DEFILP AND PRDELP=DEDELP AND PRNROP=DENROP AND PRSYST=DESYST AND PRFECT=DEFECT AND PROPED=R_OPED");
		myQuery.append(
		        " AND PRFILP=R_FILP AND PRDELP=R_DELP AND PRNROP=R_NROP AND PRSYST=R_SYST AND PRFECT=R_FECT AND DEPRES=420101 ");
		myQuery.append(" AND PRTRAC=2");
		myQuery.append(" AND PRATRC='A' ");

		myQuery.append(" AND PROPED='" + record.getTxKey().getCodigoOperador() + "'");
		myQuery.append(" AND PRFILP=" + record.getTxKey().getFilialPos());
		myQuery.append(" AND PRDELP=" + record.getTxKey().getDelegacionPos());
		myQuery.append(" AND PRNROP=" + record.getTxKey().getNroPos());
		myQuery.append(" AND PRSYST=" + record.getTxKey().getNroTx());
		// MACHEAMOS EL REGISTRO UNICO

		System.out.println(myQuery.toString());
		return myQuery.toString();
		// " AND R_LEYY not Like '%OK%'"
		// --and r_syst=728428
		// --order by PRFECT,PRHOR
	}
}
