package ar.com.osde.rules.paralelo;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class RouteMessageMatcher {
	private Map<TransactionKey, RequestRecord> forwardedRequest = new ConcurrentHashMap<TransactionKey, RequestRecord>();

	public void addRequest(String request) {
		this.forwardedRequest.put(new TransactionKey(request), new RequestRecord(request));
	}

	public RequestRecord get(TransactionKey key) {
		return this.forwardedRequest.get(key);
	}

	public int requestCount() {
		return this.forwardedRequest.keySet().size();
	}

	public void clean() {
		this.forwardedRequest.clear();
	}

	public Set<TransactionKey> getAllKeys() {
		return this.forwardedRequest.keySet();
	}
}
