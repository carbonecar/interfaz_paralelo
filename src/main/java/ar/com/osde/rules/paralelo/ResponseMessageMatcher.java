package ar.com.osde.rules.paralelo;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Verifica que cada mensaje recibido existe o no. En caso de existir verifica
 * si el mismo coincide exactamente con el que ya esta.
 * 
 * 
 * 
 * @author VA27789605
 * 
 */
public class ResponseMessageMatcher {

	private Map<TransactionKey, ResponseRecord> responses = new HashMap<TransactionKey, ResponseRecord>();

	private Map<TransactionKey, RequestRecord> requests = new HashMap<TransactionKey, RequestRecord>();

	/**
	 * Cola para saber que transacciones vamos a borrar.
	 */
	private Queue<TimedTransaction> transacciones = new ConcurrentLinkedQueue<TimedTransaction>();
	private Logger LOG = Logger.getLogger(ResponseMessageMatcher.class);

	private static final Logger LOG_SIZE = Logger.getLogger("MATCHER_SIZE");

	public static final int TOTAL_LENGTH_MESSAGE1A = 390;
	public static final int I_FIRST_HALF_MESSAGE = 0;
	public static final int E_FIRST_HALF_MESSAGE = 44;
	public static final int I_SECOND_HALF_MESSAGE = 164;
	public static final int E_SECOND_HALF_MESSAGE = 185;

	public static final int I_CODIGOS_INC = 185;
	public static final int E_CODIGOS_INC = 190;

	public static final int I_FOURTH_HALF_MESSAGE = 191;
	public static final int E_FOURTH_HALF_MESSAGE = 239;

	public static final int I_SEXO_FECHA = 269;
	public static final int E_SEXO_FECHA = 278;

	public static final int I_THIRD_HALF_MESSAGE = 393;
	public static final int E_THIRD_HALF_MESSAGE = 623;

	public static final int I_COBERTURA_1 = 404;
	public static final int I_COBERTURA_2 = 467;
	public static final int I_COBERTURA_3 = 530;

	public static final int I_ADV = 185;
	public static final int E_ADV = 190;

	private static final long MS_TO_ERASE = 60 * 1000;
	private static final int LONG_PORCENAJTES_COBERTURA = 5;

	public static final Logger LOG_RECORD_HANDLER = Logger
			.getLogger("ar.com.osde.centroValidador.pos.handlers.LoggerMessageRecordHandler");

	public static final String SEPARADOR = ",";
	public static final String MY_NAME = "ar.com.osde.centroValidador.pos.handlers.LoggerMessageRecordHandler";
	public static final String FIELD_BOOKMARK = " # ";

	/**
	 * Devuelve una respuesta con un nivel de validez.
	 * <li>NULL, Si la repuesta de apto o no apto son diferentes el nivel de
	 * validez es NULL</li>
	 * <li>VALIDAS. Si matchean como iguales se determina que son VALIDAS. Esto
	 * implica que son identicas desde: 0-44 de la 164 a la 239 (incluyendo
	 * advertencias) y de la 393 a la 623</li>
	 * <li>DIFERENCIA_COBERTURA. Son aquellas que solo dieron igual en aptitud
	 * pero difieron en las coberturas de medicamento.
	 * <li>VALIDAS_CON_DIFERENCIA_N1. Si dieron VALIDAS pero solo difirieon en
	 * las advertencias el nivel de validez es VALIDAS_CON_DIFERENCIA_N1;</li>
	 * <li>VALIDAS_CON_DIFERENCIA es cuando son validas y no estan en ninguno de
	 * los casos anteriores</>
	 * 
	 * @param request
	 * @return
	 */
	public synchronized ResponseMatcher addRequest(String request) {
		RequestRecord requestRecord = new RequestRecord(request);
		this.requests.put(requestRecord.getTransactionKey(), requestRecord);
		ResponseMatcher responseMatcher = new ResponseMatcher();
		ResponseRecord alreadyRegisteredResponse = this.responses.get(requestRecord.getTransactionKey());
		// Si el trio esta completo macheo
		if (alreadyRegisteredResponse != null && alreadyRegisteredResponse.getFirstResponse().getTextResponse() != null
				&& alreadyRegisteredResponse.getSecondResponse() != null) {
			responseMatcher = doMatch(
					buildResponseRecord(alreadyRegisteredResponse.getSecondResponse().getTextResponse(),
							alreadyRegisteredResponse.getSecondResponse().getSystemName()));
		}
		
		LOG_SIZE.info("request size:" +requests.size());
		LOG_SIZE.info("response size:" +responses.size());
		LOG_SIZE.info("transacciones a eliminar: "+transacciones.size());

		return responseMatcher;

	}

	public synchronized ResponseMatcher addResponse(String response, String systemName) {

		ResponseRecord responseRecord = buildResponseRecord(response, systemName);

		ResponseMatcher responseMatcher = doMatch(responseRecord);
		return responseMatcher;
	}

	private ResponseRecord buildResponseRecord(String response, String systemName) {
		return MainApp.createResponseRecord(response, systemName);
	}

	/**
	 * Este responseRecord no esta guardado.
	 * 
	 * @param responseRecordToBeMatched
	 * @return
	 */
	private ResponseMatcher doMatch(ResponseRecord responseRecordToBeMatched) {

		ResponseRecord alreadyRegisteredResponse = this.responses.remove(responseRecordToBeMatched.getTxKey());
		ResponseMatcher responseMatcher = new ResponseMatcher();

		RequestRecord requestRecord = this.requests.get(responseRecordToBeMatched.getTxKey());

		// Si no esta su par o no esta el request no puedo hacer el macheo
		if (alreadyRegisteredResponse == null || requestRecord == null) {
			if (alreadyRegisteredResponse == null) { // si ya esta registrado lo
														// agrego como segunda
														// respuesta
				this.responses.put(responseRecordToBeMatched.getTxKey(), responseRecordToBeMatched);
			} else { // si esta su par, pero no esta el request guardo la
						// segunda respuesta.
				alreadyRegisteredResponse.setSecondResponse(responseRecordToBeMatched.getFirstResponse());
				responses.put(alreadyRegisteredResponse.getTxKey(), alreadyRegisteredResponse);
			}
			checkRemoveAndLogOldTransactions(responseRecordToBeMatched);
			responseMatcher.setWasMatched(false);
		} else {
			realizeMatch(responseRecordToBeMatched, alreadyRegisteredResponse, responseMatcher, requestRecord);
		}
		return responseMatcher;
	}

	private void checkRemoveAndLogOldTransactions(ResponseRecord responseRecordToBeMatched) {
		this.transacciones.add(
				new TimedTransaction(System.currentTimeMillis(), MS_TO_ERASE, responseRecordToBeMatched.getTxKey()));
		TimedTransaction tTransaction = this.transacciones.peek();
		if (tTransaction.hasExpired()) {
			this.transacciones.remove(tTransaction);
			ResponseRecord response = this.responses.remove(tTransaction.getWrapee());
			RequestRecord requestRecord = this.requests.remove(tTransaction.getWrapee());

			if (requestRecord != null) {
				//
				LOG.info("se ha descartado una transaccion por falta de emparejamiento request/response1-response2");
				StringBuffer loggerLine = new StringBuffer(8192);

				DateTimeFormatter parser = ISODateTimeFormat.dateTime();
				String dateTime = parser.print(new DateTime());
				loggerLine.append("ts=" + dateTime);

				loggerLine.append(FIELD_BOOKMARK).append("request=").append(requestRecord.getRequest())
						.append(FIELD_BOOKMARK).append("response-" + ((response.getFirstResponse() == null)
								? response.getSystemNameWithoutResponse() + "=SIN RESPUESTA REGISTRADA"
								: response.getFirstResponse().getSystemName()
										.concat("=" + response.getFirstResponse().getTextResponse())))
						.append(FIELD_BOOKMARK)
						.append("response-" + ((response.getSecondResponse() == null)
								? response.getSystemNameWithoutResponse() + "=SIN RESPUESTA REGISTRADA"
								: response.getSecondResponse().getSystemName()
										.concat("=" + response.getSecondResponse().getTextResponse())))
						.append(FIELD_BOOKMARK).append("f1=0 # f2=1 # f3=0 # f4=0 # f5=0").append(FIELD_BOOKMARK)
						.append("geocoding=").append(GeoCodingBuilder.getGeoHashByFilialID(requestRecord.getRequest()));

				LOG_RECORD_HANDLER.info(loggerLine);
				LOG.info(loggerLine);
			}
		}

	}

	private void realizeMatch(ResponseRecord responseRecordToBeMatched, ResponseRecord alreadyRegisteredResponse,
			ResponseMatcher responseMatcher, RequestRecord requestRecord) {
		responseRecordToBeMatched.setRequest(requestRecord.getRequest());
		this.requests.remove(responseRecordToBeMatched.getTxKey());
		// alguna tiene request se pueden comparar
		// if (registeredResponse.getRequest() != null ||
		// responseRecord.getRequest() != null) {
		// Si ambas tienen el mismo resultado de OK/FALSE
		if (responseRecordToBeMatched.getRequest() != null) {
			responseMatcher.setResponseRecord(responseRecordToBeMatched);
			alreadyRegisteredResponse.setRequest(responseRecordToBeMatched.getRequest());

		}
		alreadyRegisteredResponse.setSecondResponse(responseMatcher.getResponseRecord().getFirstResponse());
		responseMatcher.setResponseRecord(alreadyRegisteredResponse);
		if (alreadyRegisteredResponse.getFinalResult().equals(responseRecordToBeMatched.getFinalResult())) {
			responseMatcher.setWasMatched(true);
			responseMatcher.setResult(true);
			responseMatcher.setPorcentajes(true);
			if (this.getTipoAtributo(responseRecordToBeMatched, alreadyRegisteredResponse).isValidaPorcentajes()) {
				compararPorcentajes(responseRecordToBeMatched, alreadyRegisteredResponse, responseMatcher);
			}
			if (responseMatcher.isPorcentajes()) {
				matchTransaccionCompleta(responseMatcher, responseRecordToBeMatched, alreadyRegisteredResponse);
			} else {
				StringBuffer strBuff = new StringBuffer();
				strBuff.append("\n**********************TRANSACCION  FALLIDA: ")
						.append(responseMatcher.getNivelValidez()).append(" ")
						.append(responseRecordToBeMatched.getTxKey()).append("\n");
				logRegisteredResponse(responseRecordToBeMatched, alreadyRegisteredResponse, strBuff);
				strBuff.append("******************FIN TRANSACCION FALLIDA: ").append(responseMatcher.getNivelValidez())
						.append(" ").append(responseRecordToBeMatched.getTxKey()).append("\n");
				LOG.info(strBuff.toString());
			}
		} else {
			responseMatcher.setResult(false);
			responseMatcher.setWasMatched(true);
			StringBuffer strBuff = new StringBuffer();
			strBuff.append("\n**********************TRANSACCION  FALLIDA: ")
					.append(responseRecordToBeMatched.getTxKey()).append("\n");
			logRegisteredResponse(responseRecordToBeMatched, alreadyRegisteredResponse, strBuff);
			strBuff.append("******************FIN TRANSACCION FALLIDA: ").append(responseRecordToBeMatched.getTxKey())
					.append("\n");
			LOG.info(strBuff.toString());
		}
		// }
	}

	private void logRegisteredResponse(ResponseRecord responseRecord, ResponseRecord registeredResponse,
			StringBuffer strBuff) {
		ResponseRecord respuestaAs;
		ResponseRecord respuestaMotor;
		strBuff.append("Transaccion original: ");
		if (responseRecord.getRequest() != null) {
			respuestaMotor = responseRecord;
			respuestaAs = registeredResponse;
		} else {
			respuestaMotor = registeredResponse;
			respuestaAs = responseRecord;
		}
		strBuff.append(respuestaMotor.getRequest()).append("\n");
		strBuff.append("Respuesta AS      : ").append(respuestaAs.toString()).append(" stream: ")
				.append(respuestaAs.getFirstResponse().getTextResponse()).append("\n");
		strBuff.append("Respuesta Motor   : ").append(respuestaMotor.toString()).append(" stream: ")
				.append(respuestaMotor.getFirstResponse().getTextResponse()).append("\n");
	}

	private void compararPorcentajes(ResponseRecord responseRecord, ResponseRecord registeredResponse,
			ResponseMatcher responseMatcher) {

		String registeredResponseStr = registeredResponse.getFirstResponse().getTextResponse();
		String responseRecordStr = responseRecord.getFirstResponse().getTextResponse();
		if (responseRecordStr == null || registeredResponseStr == null) {
			responseMatcher.setPorcentajes(false);
		} else {
			if (registeredResponseStr.length() < 540 || responseRecordStr.length() < 540) {
				responseMatcher.setPorcentajes(false);
			} else {
				String porcentaje1 = responseRecordStr.substring(I_COBERTURA_1,
						I_COBERTURA_1 + LONG_PORCENAJTES_COBERTURA);
				String porcentaje2 = responseRecordStr.substring(I_COBERTURA_2,
						I_COBERTURA_2 + LONG_PORCENAJTES_COBERTURA);
				String porcentaje3 = responseRecordStr.substring(I_COBERTURA_3,
						I_COBERTURA_3 + LONG_PORCENAJTES_COBERTURA);

				String porcentaje11 = registeredResponseStr.substring(I_COBERTURA_1,
						I_COBERTURA_1 + LONG_PORCENAJTES_COBERTURA);
				String porcentaje21 = registeredResponseStr.substring(I_COBERTURA_2,
						I_COBERTURA_2 + LONG_PORCENAJTES_COBERTURA);
				String porcentaje31 = registeredResponseStr.substring(I_COBERTURA_3,
						I_COBERTURA_3 + LONG_PORCENAJTES_COBERTURA);

				if (porcentaje1.equals(porcentaje11) && porcentaje2.equals(porcentaje21)
						&& porcentaje3.equals(porcentaje31)) {
					responseMatcher.setPorcentajes(true);
				} else {
					responseMatcher.setPorcentajes(false);
				}
			}
		}

	}

	private TipoAtributo getTipoAtributo(ResponseRecord responseRecord, ResponseRecord registeredResponse) {
		String request = null;
		if (registeredResponse.getRequest() != null) {
			request = registeredResponse.getRequest();
		} else {
			request = responseRecord.getRequest();
		}
		String tipoTransaccion = request.substring(45, 47);
		Integer codigo = Integer.parseInt(tipoTransaccion.substring(0, 1));
		String atributo = tipoTransaccion.substring(1, 2);
		TipoAtributo tipoAtributo = new TipoAtributo(atributo, codigo);
		return tipoAtributo;
	}

	private ResponseMatcher matchTransaccionCompleta(ResponseMatcher responseMatcher, ResponseRecord responseRecord,
			ResponseRecord registeredResponse) {
		boolean firstHalfFail;
		boolean secondHalfFail;
		boolean thirdHalfFail;
		boolean fourthHalfFail;
		boolean advertenciasFail;

		boolean sexoFechaNacimientoFail;

		responseMatcher.setWasMatched(true);
		responseMatcher.setResult(true);
		responseMatcher.setPorcentajes(true);
		StringBuffer strBuff = new StringBuffer(10000);

		String firstHalfMessageRegisteredResponse = registeredResponse.getFirstResponse().getTextResponse()
				.substring(I_FIRST_HALF_MESSAGE, E_FIRST_HALF_MESSAGE);
		String firstHalfMessageResponseRecord = responseRecord.getFirstResponse().getTextResponse()
				.substring(I_FIRST_HALF_MESSAGE, E_FIRST_HALF_MESSAGE);

		String secondHalfMessageRegisteredResponse = registeredResponse.getFirstResponse().getTextResponse()
				.substring(I_SECOND_HALF_MESSAGE, E_SECOND_HALF_MESSAGE);
		String secondHalfMessageResponseRecord = responseRecord.getFirstResponse().getTextResponse()
				.substring(I_SECOND_HALF_MESSAGE, E_SECOND_HALF_MESSAGE);

		String thirdHalfMessageRegisteredResponse = registeredResponse.getFirstResponse().getTextResponse()
				.substring(I_THIRD_HALF_MESSAGE, E_THIRD_HALF_MESSAGE);
		String thirdHalfMessageResponseRecord = safeSubstring(responseRecord.getFirstResponse().getTextResponse(),
				I_THIRD_HALF_MESSAGE, E_THIRD_HALF_MESSAGE);

		String fourthHalfMessageRegisteredResponse = registeredResponse.getFirstResponse().getTextResponse()
				.substring(I_FOURTH_HALF_MESSAGE, E_FOURTH_HALF_MESSAGE);
		String fourthHalfMessageResponseRecord = responseRecord.getFirstResponse().getTextResponse()
				.substring(I_FOURTH_HALF_MESSAGE, E_FOURTH_HALF_MESSAGE);

		String advertenciasRegisteredResponse = registeredResponse.getFirstResponse().getTextResponse().substring(I_ADV,
				E_ADV);
		String advertenciasResponseRecord = responseRecord.getFirstResponse().getTextResponse().substring(I_ADV, E_ADV);

		firstHalfFail = !firstHalfMessageRegisteredResponse.equals(firstHalfMessageResponseRecord);
		secondHalfFail = !secondHalfMessageRegisteredResponse.equals(secondHalfMessageResponseRecord);
		thirdHalfFail = !thirdHalfMessageRegisteredResponse.equals(thirdHalfMessageResponseRecord);
		fourthHalfFail = !fourthHalfMessageRegisteredResponse.equals(fourthHalfMessageResponseRecord);
		advertenciasFail = !advertenciasRegisteredResponse.equals(advertenciasResponseRecord);

		String sexoFechaNacimientoRegisteredResponse = registeredResponse.getFirstResponse().getTextResponse()
				.substring(I_SEXO_FECHA, E_SEXO_FECHA);
		String sexoFechaNacimientoResponseRecord = responseRecord.getFirstResponse().getTextResponse()
				.substring(I_SEXO_FECHA, E_SEXO_FECHA);

		sexoFechaNacimientoFail = !sexoFechaNacimientoRegisteredResponse.equals(sexoFechaNacimientoResponseRecord);
		if (!this.getTipoAtributo(responseRecord, registeredResponse).isFarmacia()) {
			thirdHalfFail = false;
		}
		if (firstHalfFail || secondHalfFail || thirdHalfFail || fourthHalfFail || sexoFechaNacimientoFail
				|| advertenciasFail) {
			responseMatcher.setFullResult(false);
			// Si solo fallaron los codigos de advertencias lo ponemos
			if (!(firstHalfFail || secondHalfFail || thirdHalfFail || fourthHalfFail || sexoFechaNacimientoFail)) {
				responseMatcher.setOnlyAdvertenciasFail(true);
			}
			strBuff.append("\n**********************TRANSACCION COMPLETA FALLIDA: ")
					.append(responseMatcher.getNivelValidez()).append(" ").append(responseRecord.getTxKey())
					.append("\n");
			logRegisteredResponse(responseRecord, registeredResponse, strBuff);
			strBuff.append("******************FIN TRANSACCION COMPLETA FALLIDA: ")
					.append(responseMatcher.getNivelValidez()).append(" ").append(responseRecord.getTxKey())
					.append("\n");
			LOG.info(strBuff.toString());
		} else {
			responseMatcher.setFullResult(true);
		}

		return responseMatcher;
	}

	private class TimedTransaction {
		private long time;
		private TransactionKey transactionKey;
		private long expiredTime;

		public TimedTransaction(long time, long expiredTime, TransactionKey transactionKey) {
			this.time = time;
			this.expiredTime = expiredTime;
			this.transactionKey = transactionKey;
		}

		public TransactionKey getWrapee() {
			return this.transactionKey;
		}

		public boolean hasExpired() {
			if ((System.currentTimeMillis() - this.time) > this.expiredTime) {
				return true;
			}
			return false;
		}
	}

	private class TipoAtributo {
		private String tipoTransaccion;
		private Integer atributo;
		private boolean validaPorcentajes = false;

		private TipoAtributo(String tipoTransaccion, Integer atributo) {
			super();
			this.tipoTransaccion = tipoTransaccion;
			this.atributo = atributo;
		}

		public boolean isFarmacia() {
			if ("F".equals(tipoTransaccion) || "J".equals(tipoTransaccion)) {
				return true;
			}
			return false;
		}

		public boolean isValidaPorcentajes() {
			if ("F".equals(tipoTransaccion)) {
				if (atributo == 2 || atributo == 1) {
					validaPorcentajes = true;
				}
			}
			return validaPorcentajes;
		}

	}

	private String safeSubstring(String string, int begin, int end) {
		String returnValue = "";
		try {
			returnValue = string.substring(begin, end);
		} catch (IndexOutOfBoundsException ioExcption) {
			// nothing to do
		}
		return returnValue;
	}

}
