package ar.com.osde.rules.paralelo;

import java.util.HashMap;
import java.util.Map;

public class GeoCodingBuilder {
	/**
	 * Devuelve el geohash de un codigo de filial. Espera un REQUEST DE POS
	 * valido
	 * 
	 * @param strRequestPOS
	 *            Un request de POS valido.}
	 * @author luisgarciabustos
	 */
	public static String getGeoHashByFilialID(String strRequestPOS) {
		Map<String, String> map = new HashMap<String, String>();

		map.put("01", "6dcnnchhp");
		map.put("02", "6d6md3e26");
		map.put("03", "6en5szteg");
		map.put("04", "6en5cephw");
		map.put("05", "6839cy16s");
		map.put("06", "6dkgqwyc2");
		map.put("07", "6eqeh6h92");
		map.put("08", "6e9w6h9wd");
		map.put("09", "696r9jmdu");
		map.put("10", "6db2wtqsh");
		map.put("11", "66phck2b2");
		map.put("12", "6g0k24062");
		map.put("13", "63pd7jqv8");
		map.put("14", "69tzz4wbs");
		map.put("15", "6e96zx7f9");
		map.put("16", "66rk89frm");
		map.put("17", "6d0fd3tn1");
		map.put("18", "4qyf34b5n");
		map.put("19", "6dku48q1j");
		map.put("20", "6e43cv3cz");
		map.put("21", "6e1xk4vm5");
		map.put("22", "4qry52zdy");
		map.put("23", "6dhuef3m7");
		map.put("24", "69ppf3gn8");
		map.put("25", "69yc34v7m");
		map.put("26", "4xb5072q6");
		map.put("30", "62tmcpyuv");
		map.put("31", "695ez0kxw");
		map.put("32", "69mgwmp0t");
		map.put("33", "69uzjtd15");
		map.put("35", "69v36r2q2");
		map.put("36", "69e7k7ms5");
		map.put("37", "69uejnjnm");
		map.put("39", "6d5p8ggpp");
		map.put("40", "6d45x9mmg");
		map.put("42", "6dqtru98q");
		map.put("60", "69y7pwng3");
		map.put("90", "69y7pt1hw");

		return map.get(strRequestPOS.substring(7, 9));
	}

}
