package ar.com.osde.rules.paralelo;

public enum NivelValidez {
    // Las validas con diferencia N1 son aquellas que por negocio siempre van a
    // tener una diferencia.
    // dicha diferencia esta identificada y fue validada.
    VALIDAS("VALIDAS"), DIFERENCIA_APTITUD("Diferencia Aptitud"), DIFERENCIA_COBERTURA("Diferencia Cobertura"), VALIDAS_CON_DIFERENCIA(
            "Validas con Diferencia"), VALIDAS_CON_DIFERENCIA_N1("Validas con Diferencia N1");

    private String descripcion;

    private NivelValidez(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescription() {
        return this.descripcion;
    }
}
