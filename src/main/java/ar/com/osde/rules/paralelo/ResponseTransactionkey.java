package ar.com.osde.rules.paralelo;

import ar.com.osde.rules.paralelo.ResponseRecord;

public class ResponseTransactionkey extends TransactionKey {

	public ResponseTransactionkey(String stringResponse) {

		String[] myTx = new String[8];


		myTx[0] = stringResponse.substring(5, 7);
		myTx[1] = stringResponse.substring(7, 9);
		myTx[2] = stringResponse.substring(9, 11);
		myTx[3] = stringResponse.substring(11, 15);
		myTx[4] = stringResponse.substring(15, 21);
		myTx[6] = stringResponse.substring(175, 177);
		myTx[7] = stringResponse.substring(177, 179);

	
	
		assignKeyValues(myTx[ResponseRecord.CODIGO_OPERADOR], myTx[ResponseRecord.FILIAL],
		        myTx[ResponseRecord.DELEGACION], myTx[ResponseRecord.NRO_POS], myTx[ResponseRecord.NRO_TRX],
		        myTx[ResponseRecord.NRO_SEC_MENSAJE], myTx[ResponseRecord.NRO_TOTAL_MENSAJE]);


	}

}
