package ar.com.osde.rules.paralelo;

public class ResponseRecord {

	public static final int CODIGO_OPERADOR = 0;
	public static final int FILIAL = 1;
	public static final int DELEGACION = 2;
	public static final int NRO_POS = 3;
	public static final int NRO_TRX = 4;

	public static final int CODIGO_RESPUESTA = 5;
	public static final int NRO_SEC_MENSAJE = 6;
	public static final int NRO_TOTAL_MENSAJE = 7;
	private TransactionKey txKey;
	private Boolean finalResult;
	private int hora;

	private String request;
	private Response firstResponse;
	private Response secondResponse;

	private long timestamp;

	public ResponseRecord(String stringResponse) {

		String[] myTx = new String[8];
		myTx[5] = stringResponse.substring(34, 36);

		this.txKey = new ResponseTransactionkey(stringResponse);
		this.finalResult = booleanValue(myTx[ResponseRecord.CODIGO_RESPUESTA]);

		this.timestamp = System.currentTimeMillis();

	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Response getFirstResponse() {
		return firstResponse;
	}

	public void setFirstResponse(Response firstResponse) {
		this.firstResponse = firstResponse;
		this.firstResponse.measureResposneTime(this.timestamp);
	}

	public Response getSecondResponse() {
		return secondResponse;
	}

	public void setSecondResponse(Response secondResponse) {
		this.secondResponse = secondResponse;
		this.secondResponse.measureResposneTime(this.timestamp);
	}

	public long getTimeStamp() {
		return timestamp;
	}

	@Override
	public String toString() {

		return this.txKey + " Respuesta: " + this.finalResult;
	}

	public int getHora() {
		return hora;
	}

	public void setHora(int hora) {
		this.hora = hora;
	}

	public TransactionKey getTxKey() {
		return txKey;
	}

	public void setTxKey(TransactionKey txKey) {
		this.txKey = txKey;
	}

	public Boolean getFinalResult() {
		return finalResult;
	}

	public void setFinalResult(Boolean finalResult) {
		this.finalResult = finalResult;
	}

	/**
	 * Devuelve el nombre el sistema del que se estima que falta la respuesta
	 * 
	 * @return
	 */
	public String getSystemNameWithoutResponse() {
		if (this.firstResponse == null) {
			return extractSystemWithoutResponse(this.secondResponse);
		}

		if (this.secondResponse == null) {
			return this.extractSystemWithoutResponse(this.firstResponse);
		}
		return "sin sistema";
	}

	public Response getResonseBySystemName(SystemName system) {
		if (this.firstResponse.getSystemName().equals(system.getName())) {
			return this.firstResponse;
		} else {
			return this.secondResponse;
		}
	}

	private String extractSystemWithoutResponse(Response responseRecord) {
		if (responseRecord.getSystemName().equals(SystemName.POS.getName())) {
			return SystemName.SALUD_SOFT.getName();
		} else {
			return SystemName.POS.getName();
		}
	}

	private static Boolean booleanValue(String string) {
		if ("00".equals(string)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public enum SystemName {

		POS("pos"), SALUD_SOFT("ss");

		private String name;

		SystemName(String name) {
			this.name = name;
		}

		public String toString() {
			return name;
		}

		public String getName() {
			return name;
		}
	}
}
