package ar.com.osde.rules.paralelo;

public class TransactionKey {

	private String codigoOperador;

	private int filialPos;
	private int delegacionPos;
	private int nroPos;
	private int nroTx;
	private int nroSecuenciaMensaje;
	private int nroTotalMensaje;

	@Override
	public String toString() {
		return "Codigo Operador: " + codigoOperador + " filialPos: " + filialPos + " delegacionPos: " + delegacionPos
				+ " nroPos: " + nroPos + " nroTrx: " + nroTx + " nroSecuenciaMensaje: "+nroSecuenciaMensaje+ " nroTotalMensajes: "+nroTotalMensaje;
	}

	public TransactionKey() {

	}

	public TransactionKey(String stringResponse) {

		String[] myTx = new String[7];

		myTx[0] = stringResponse.substring(5, 7); // operador

		myTx[1] = stringResponse.substring(7, 9);// filial
		myTx[2] = stringResponse.substring(9, 11);// delegacion
		myTx[3] = stringResponse.substring(11, 15);// nroPos
		myTx[4] = stringResponse.substring(47, 53);// NroTrx

		myTx[5] = stringResponse.substring(178, 180);// numero de secuencia de mensaje
		myTx[6] = stringResponse.substring(180, 182);// numero total de mensaje

		codigoOperador = myTx[RequestRecord.CODIGO_OPERADOR];
		try {
			filialPos = Integer.parseInt(myTx[RequestRecord.FILIAL]);
		} catch (NumberFormatException e) {
			filialPos = 0;
		}
		try {
			delegacionPos = Integer.parseInt(myTx[RequestRecord.DELEGACION]);

		} catch (NumberFormatException e) {
			delegacionPos = 0;
		}

		try {
			nroPos = Integer.parseInt(myTx[RequestRecord.NRO_POS]);
		} catch (NumberFormatException e) {
			nroPos = 0;
		}
		try {
			nroTx = Integer.parseInt(myTx[RequestRecord.NRO_TRX]);
		} catch (NumberFormatException e) {
			nroTx = 0;
		}
		this.nroSecuenciaMensaje = this.stringToint(myTx[RequestRecord.NRO_SEC_MENSAJE]);
		this.nroTotalMensaje = this.stringToint(myTx[RequestRecord.NRO_TOTAL_MENSAJE]);
	}

	protected void assignKeyValues(String codigoOperador, String filialPos, String delegacionPos, String nroPos,
	        String nroTx, String nroSecMensaje, String nroTotalMensaje) {
		this.codigoOperador = codigoOperador;
		this.filialPos = this.stringToint(filialPos);
		this.delegacionPos = this.stringToint(delegacionPos);
		this.nroPos = this.stringToint(nroPos);
		this.nroTx = this.stringToint(nroTx);

		this.nroSecuenciaMensaje = this.stringToint(nroSecMensaje);
		this.nroTotalMensaje = this.stringToint(nroTotalMensaje);
	}

	public String getCodigoOperador() {
		return codigoOperador;
	}

	public void setCodigoOperador(String codigoOperador) {
		this.codigoOperador = codigoOperador;
	}

	public int getFilialPos() {
		return filialPos;
	}

	public void setFilialPos(int filialPos) {
		this.filialPos = filialPos;
	}

	public int getDelegacionPos() {
		return delegacionPos;
	}

	public void setDelegacionPos(int delegacionPos) {
		this.delegacionPos = delegacionPos;
	}

	public int getNroPos() {
		return nroPos;
	}

	public void setNroPos(int nroPos) {
		this.nroPos = nroPos;
	}

	public int getNroTx() {
		return nroTx;
	}

	public void setNroTx(int nroTx) {
		this.nroTx = nroTx;
	}

	public int getNroSecuenciaMensaje() {
		return nroSecuenciaMensaje;
	}

	public void setNroSecuenciaMensaje(int nroSecuenciaMensaje) {
		this.nroSecuenciaMensaje = nroSecuenciaMensaje;
	}

	public int getNroTotalMensaje() {
		return nroTotalMensaje;
	}

	public void setNroTotalMensaje(int nroTotalMensaje) {
		this.nroTotalMensaje = nroTotalMensaje;
	}

	@Override
	public boolean equals(Object paramObject) {
		if (!(paramObject instanceof TransactionKey)) {
			return false;
		}
		boolean isEqual = false;
		TransactionKey trxKey = (TransactionKey) paramObject;

		isEqual = trxKey.getCodigoOperador().equals(this.getCodigoOperador());
		isEqual = isEqual && (trxKey.getDelegacionPos() == this.getDelegacionPos());
		isEqual = isEqual && (trxKey.getFilialPos() == this.getFilialPos());
		isEqual = isEqual && (trxKey.getNroPos() == this.getNroPos());
		isEqual = isEqual && (trxKey.getNroTx() == this.getNroTx());
		isEqual = isEqual && (trxKey.getNroSecuenciaMensaje() == this.getNroSecuenciaMensaje());
		isEqual = isEqual && (trxKey.getNroTotalMensaje() == this.getNroTotalMensaje());
		return isEqual;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoOperador == null) ? 0 : codigoOperador.hashCode());
		result = prime * result + delegacionPos;
		result = prime * result + filialPos;
		result = prime * result + nroPos;
		result = prime * result + nroTx;
		result = prime * result + nroTotalMensaje;
		result = prime * result + nroSecuenciaMensaje;
		return result;
	}

	private int stringToint(String number) {
		int safeValue = 0;
		try {
			safeValue = Integer.parseInt(number);
		} catch (NumberFormatException e) {
			// nothing to do
		}
		return safeValue;
	}
}
